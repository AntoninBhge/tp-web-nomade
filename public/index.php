<?php

use DI\ContainerBuilder;
use GuzzleHttp\Psr7\UploadedFile;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;
use Slim\Factory\AppFactory;


require __DIR__ . '/../vendor/autoload.php';

$containerBuilder = new ContainerBuilder();
$container = $containerBuilder->build();

$container->set('upload_directory', __DIR__ . '\uploads');

AppFactory::setContainer($container);
$app = AppFactory::create();

$app->post('/', function (ServerRequestInterface $request, ResponseInterface $response) {
    $directory = $this->get('upload_directory');
    $uploadedFiles = $request->getUploadedFiles();

    foreach ($uploadedFiles as $file) {
        if ($file->getError() === UPLOAD_ERR_OK) {
            $filename = moveUploadedFile($directory, $file);
            $response->getBody()->write('Uploaded: ' . $filename . '<br/>');
        } else {
            $response->withStatus(400);
        }
    }

    return $response;
});

$app->get('/', function (ServerRequestInterface $request, ResponseInterface $response) {
    $directory = $this->get('upload_directory');
    $list = glob($directory . '/*');
    $newList = [];
    foreach($list as $file) {
        array_push($newList, basename($file));
    }
    $response->getBody()->write(json_encode($newList));
    return $response;
});

$app->delete('/{fileName}', function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
    $directory = $this->get('upload_directory');
    $fileName = $args['fileName'];
    $path = $directory . "\\" . $fileName . ".pdf";
    $success = unlink($path);
    if ($success) {
        $response->withStatus(201);
    } else {
        $response->withStatus(400);
    }
    return $response;    
});

function moveUploadedFile(string $directory, UploadedFileInterface $uploadedFile)
{
    $filename = $uploadedFile->getClientFilename();
    $uploadedFile->moveTo($directory . "\\" . $filename);
    

    return $filename;
}

$app->run();
