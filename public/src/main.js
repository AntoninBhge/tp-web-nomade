let url = "http://localhost:8081"

function formSubmition(event) {
    let input = document.getElementById('file')
    let file = input.files[0]
    if (file !== undefined) {
        const formData = new FormData()
        formData.append(file.name, file)

        axios.post(url, formData).then(resp => {
            console.log(resp)
            createTab();
        }).catch(err => {
            console.error(err)
        })

    }
    event.preventDefault()
}

function createTab() {

    let array = document.getElementById('table')
        while (array.childElementCount > 0) {
            array.removeChild(array.lastChild)
        }
    axios.get(url).then(res => {
        console.log(res)
        let cpt = 1
        res.data.forEach(element => {
            let fileName = element.split(".")[0]
            let newRow = document.createElement('tr')
            newRow.innerHTML = '<th scope="row">' + cpt + '</th><td>' + element + '</td><td><button class="btn btn-outline-danger" onclick="deleteFile(`' + fileName + '`)" >Delete</button></td>'
            array.appendChild(newRow)
            cpt++
        })
    })
    .catch(err => console.error(err))
}

function deleteFile(fileName) {
    axios.delete(url + '/' + fileName)
    .then(res => createTab())
    .catch(err => console.error(err))
}

document.getElementById('form').addEventListener('submit', formSubmition)
createTab()
